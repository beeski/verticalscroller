﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyFactory : MonoBehaviour 
{
	private static EnemyFactory mInstance; 

	[SerializeField] private Camera GameplayCamera;
	[SerializeField] private Material EnemyMaterial; 
	[SerializeField] private float EnemyScale = 1.5f; 
	[Range( 1, 100 )]
	[SerializeField] private int EnemyPoolSize = 10; 
	
	private GameObject [] mEnemyPool;
	private List<GameObject> mActiveEnemys;
	private List<GameObject> mAvailableEnemys;
	private float mColumnSize;
	
	void Awake()
	{
		if( mInstance == null )
		{
			mInstance = this;

			mColumnSize = ( Gameplay.ScreenHeight * GameplayCamera.aspect * 0.8f ) / 3;

			// Create the enemies, initialise the active and available lists, put all enemies in the available list
			mActiveEnemys = new List<GameObject>();
			mAvailableEnemys = new List<GameObject>();
			mEnemyPool = new GameObject[EnemyPoolSize];
			for( int count = 0; count < EnemyPoolSize; count++ )
			{
				GameObject enemy = new GameObject( "Enemy_PoolID" + ( count + 1 ) );
				CreateMesh m = enemy.AddComponent<CreateMesh>();
				m.Material = EnemyMaterial;
				enemy.transform.localScale = new Vector3( EnemyScale, EnemyScale, EnemyScale );
				enemy.transform.localRotation = Quaternion.AngleAxis( 180.0f, Vector3.forward );
				enemy.transform.parent = transform;
				mAvailableEnemys.Add( enemy );
				enemy.SetActive( false );
			}
		}
		else
		{
			Debug.LogError( "Only one EnemyFactory allowed - destorying duplicate" );
			Destroy( this.gameObject );
		}
	}

	public static GameObject Dispatch( int column )
	{
		if( mInstance != null )
		{
			return mInstance.DoDispatch( column );
		}
		return null;
	}

	public static bool Return( GameObject enemy )
	{
		if( mInstance != null )
		{
			if( mInstance.mActiveEnemys.Remove( enemy ) )
			{
				enemy.SetActive( false );
				mInstance.mAvailableEnemys.Add( enemy ); 
			}
		}
		return false;
	}

	public static void Reset()
	{
		if( mInstance != null )
		{
			for( int count = 0; count < mInstance.mActiveEnemys.Count; count++ )
			{
				mInstance.mActiveEnemys[count].SetActive( false );
				mInstance.mAvailableEnemys.Add( mInstance.mActiveEnemys[count] ); 
			}

			mInstance.mActiveEnemys.Clear();
		}
	}

	private GameObject DoDispatch( int column )
	{
		// Look for a free enemy and then dispatch them 
		GameObject result = null;
		if( mAvailableEnemys.Count > 0 )
		{
			GameObject enemy = mAvailableEnemys[0];
			Vector3 position = enemy.transform.position;
			position.x = -mColumnSize + ( mColumnSize * column ); 
			position.y = Gameplay.ScreenHeight * 0.5f;
			position.z = 0.0f;
			enemy.transform.position = position;
			enemy.SetActive( true );
			mActiveEnemys.Add( enemy );
			mAvailableEnemys.Remove( enemy );
			result = enemy;
		}
		
		// Returns true if a free enemy was found and dispatched
		return result;
	}
}
