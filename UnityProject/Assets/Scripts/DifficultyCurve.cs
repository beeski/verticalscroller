﻿using UnityEngine;
using System;
using System.Collections;

public class DifficultyCurve : MonoBehaviour 
{
	[SerializeField] private float GameStartSpeed = 10f;
	[SerializeField] private float GameSpeedRamp = 0.1f;
	[SerializeField] private float PlayerStartSpeed = 20f;
	[SerializeField] private float PlayerSpeedRamp = 0.1f;
	[SerializeField] private float BulletStartSpeed = 20f;
	[SerializeField] private float BulletSpeedRamp = 0.1f;
	[SerializeField] private float TimeBetweenRows = 5.0f;
	[SerializeField] private float TimeBetweenWaves = 40.0f;

	private Wave[] mWaves;
	private float mTimeToNextRow;
	private float mTimeToNextWave;
	private int mCurrentRow;
	private int mCurrentWave;

	public static float GameSpeed { get; private set; }
	public static float PlayerSpeed { get; private set; }
	public static float BulletSpeed { get; private set; }

	void Awake()
	{
		Reset();

		Wave [] waves = { 
			new Wave( 1, 1 ), 
			new Wave( 1, 2 ), 
			new Wave( 1, 3 ), 
			new Wave( 1, 4 ), 
			new Wave( 2, 3 ), 
			new Wave( 2, 4 ), 
			new Wave( 2, 5 ), 
			new Wave( 3, 3 ), 
			new Wave( 3, 4 ), 
			new Wave( 3, 5 ), 
			new Wave( 3, 6 ), 
			new Wave( 3, 8 ) 
		};

		mWaves = waves;
	}

	void Start()
	{
		GameSpeed = GameStartSpeed;
		PlayerSpeed = PlayerStartSpeed;
		BulletSpeed = BulletStartSpeed;
	}

	public int SpawnCount()
	{
		int enemiesToSpawn = 0;

		if( mCurrentRow < mWaves[mCurrentWave].NumberOfRows )
		{
			mTimeToNextRow -= Gameplay.GameDeltaTime;
			if( mTimeToNextRow <= 0.0f )
			{
				mCurrentRow++;
				enemiesToSpawn = mWaves[mCurrentWave].EnemiesPerRow;
				mTimeToNextRow = TimeBetweenRows;
			}
		}
		else
		{
			mTimeToNextWave -= Gameplay.GameDeltaTime;
			if( mTimeToNextWave <= 0.0f )
			{
				if( ( mCurrentWave + 1 ) < mWaves.Length )
				{
					GameSpeed += GameSpeedRamp;
					PlayerSpeed += PlayerSpeedRamp;
					BulletSpeed += BulletSpeedRamp;
					mCurrentWave++;
				}
				mTimeToNextWave = TimeBetweenWaves;
				mCurrentRow = 0;
			}
		}

		return enemiesToSpawn;
	}

	public void Stop()
	{
		GameSpeed = 0.0f;
		PlayerSpeed = 0.0f;
		BulletSpeed = 0.0f;
	}

	public void Reset()
	{
		GameSpeed = GameStartSpeed;
		PlayerSpeed = PlayerStartSpeed;
		BulletSpeed = BulletStartSpeed;
		mTimeToNextRow = TimeBetweenRows;
		mTimeToNextWave = TimeBetweenWaves;
		mCurrentRow = 0;
		mCurrentWave = 0;
	}
}
